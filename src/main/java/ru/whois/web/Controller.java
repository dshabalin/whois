package ru.whois.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.whois.TemporaryModel;
import ru.whois.repository.Repository;

@RestController
@RequestMapping("api/whois")
public class Controller {

  @Autowired
  Repository repository;

  @PostMapping
  public TemporaryModel whois(@RequestBody Request request) {
    return repository.findDomainInfo(request.domain);
  }

}
