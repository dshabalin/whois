package ru.whois.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.whois.TemporaryModel;

@org.springframework.stereotype.Repository
public class Repository {

  private static Logger logger = Logger.getLogger(Repository.class.getName());

  @Autowired
  private JdbcTemplate template;

  private String findDomainInfo = "select * from r_domain where name = ?";

  public TemporaryModel findDomainInfo(String domain) {
    logger.log(Level.INFO, "searching by domain: " + domain);
    try {
      TemporaryModel temporaryModel = template.queryForObject(
          findDomainInfo,
          new Object[]{domain},
          new DomainRowMapper()
      );
      logger.log(Level.INFO, "found: " + temporaryModel);
      return temporaryModel;
    } catch (EmptyResultDataAccessException e) {
      logger.log(Level.INFO, "not found by domain: " + domain);

      return null;
    }
  }

}

class DomainRowMapper implements RowMapper<TemporaryModel> {

  @Override
  public TemporaryModel mapRow(ResultSet rs, int i) throws SQLException {
    return new TemporaryModel(
        rs.getLong("id"),
        rs.getLong("oid"),
        rs.getString("name"),
        rs.getLong("tld"),
        rs.getString("registrant"),
        rs.getString("c_admin"),
        rs.getString("c_tech"),
        rs.getString("c_bill"),
        rs.getDate("expired")
    );
  }
}