package ru.whois.model;

import lombok.Data;

@Data
public class Agent {

  String NIC;
  String name;
  String phone;
  String fax;
  String email;
}

